﻿using System.Collections;
using System.Collections.Generic;
using HawkOMS.Models.Accounting;
using HawkOMS.Models.Grouping;
using HawkOMS.Models.User;
using Microsoft.AspNetCore.Identity;

namespace HawkOMS.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        // We'll auto-generate it even for non-recruits.
        public int FourD { get; set; }
        
        public int SectionId { get; set; }
        public Section Section { get; set; }
        
        public int RankId { get; set; }
        public Rank Rank { get; set; }
        
        public ICollection<PlatoonCommander> PlatoonCommanders { get; set; }
        public ICollection<PlatoonStrengthMissingUser> PlatoonStrengthMissingUsers { get; set; }
        public ICollection<SectionCommander> SectionCommanders { get; set; }
        public ICollection<Status> Statuses { get; set; }
        
        public ICollection<CompanyStrength> CompanyStrengths { get; set; }
    }
}