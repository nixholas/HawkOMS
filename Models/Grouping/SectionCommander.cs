﻿namespace HawkOMS.Models.Grouping
{
    public class SectionCommander
    {
        public int SectionId { get; set; }
        public Section Section { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
    }
}