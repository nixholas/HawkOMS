﻿using System.Collections.Generic;

namespace HawkOMS.Models.Grouping
{
    public class Section
    {
        public int Id { get; set; }
        
        public int PlatoonId { get; set; }
        public Platoon Platoon { get; set; }
        
        public int Number { get; set; }
        
        public ICollection<ApplicationUser> SectionUsers { get; set; }
        public ICollection<SectionCommander> SectionCommanders { get; set; }
    }
}