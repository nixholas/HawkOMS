﻿using System.Collections;
using System.Collections.Generic;

namespace HawkOMS.Models.Grouping
{
    public class Platoon
    {
        public int Id { get; set; }
        
        public int InitialTotalStrength { get; set; }
        
        public ICollection<PlatoonCommander> PlatoonCommanders { get; set; }
        public ICollection<Section> Sections { get; set; }
    }
}