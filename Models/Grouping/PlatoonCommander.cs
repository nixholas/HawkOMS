﻿namespace HawkOMS.Models.Grouping
{
    public class PlatoonCommander
    {
        public int PlatoonId { get; set; }
        public Platoon Platoon { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
    }
}