﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace HawkOMS.Models.Accounting
{
    public class PlatoonStrength
     {
         public long Id { get; set; }
         
         public DateTime CreatedAt { get; set; }
         
         public int CurrentTotal { get; set; }
         
         public long CompanyStrengthId { get; set; }
         public CompanyStrength CompanyStrength { get; set; }
         
         public ICollection<PlatoonStrengthMissingUser> PlatoonStrengthMissingUsers { get; set; }
     }
 }