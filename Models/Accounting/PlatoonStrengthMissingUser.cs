﻿namespace HawkOMS.Models.Accounting
{
    public class PlatoonStrengthMissingUser
    {
        public long Id { get; set; }
        
        public string UserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        
        public long PlatoonStrengthId { get; set; }
        public PlatoonStrength PlatoonStrength { get; set; }
    }
}