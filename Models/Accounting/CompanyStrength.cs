﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace HawkOMS.Models.Accounting
{
    public class CompanyStrength
    {
        public long Id { get; set; }
        
        public DateTime CreatedAt { get; set; }
        
        public int CurrentTotal { get; set; }
        
        public ICollection<PlatoonStrength> PlatoonStrengths { get; set; }
        
        public string CreatedById { get; set; }
        public ApplicationUser CreatedBy { get; set; }
    }
}