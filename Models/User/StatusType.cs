﻿namespace HawkOMS.Models.User
{
    public enum StatusType
    {
        ALLCLEAR = 0,
        RMJ = 1,
        LIGHT_DUTY = 2,
        HEAVY_LOAD = 3,
        UPPER_LIMB = 4,
        LOWER_LIMB = 5,
        LIVE_FIRING = 6,
        LOUD_NOISE = 7,
        BLANKS = 8,
        EXPLOSIVES = 9,
        SHARP_OBJECTS = 10,
        DIVING = 11
    }
}