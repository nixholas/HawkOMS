﻿using System;
using Serilog.Events;

namespace HawkOMS.Models.User
{
    public class Status
    {
        public long Id { get; set; }
        
        /// <summary>
        /// Let the commanders decide what level to phase someone
        /// out of the activity.
        /// </summary>
        public int Level { get; set; }
        
        public StatusType StatusType { get; set; }
        
        /// <summary>
        /// Color of the status
        /// </summary>
        public string Hex { get; set; }
        
        public string Name { get; set; }
        
        public DateTime StartDate { get; set; }
        
        public DateTime? EndDate { get; set; }
        
        public string UserId { get; set; }
        
        public ApplicationUser User { get; set; }
    }
}