﻿using System.Collections.Generic;

namespace HawkOMS.Models.User
{
    public class Rank
    {
        public int Id { get; set; }
        
        public int level { get; set; }
        
        public string Abbreviation { get; set; }
        
        public string Name { get; set; }
        
        public string IconUrl { get; set; }
        
        public ICollection<ApplicationUser> Users { get; set; }
    }
}