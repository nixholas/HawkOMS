﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace HawkOMS.Data.Migrations
{
    public partial class r1_InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FourD",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RankId",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SectionId",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "CompanyStrengths",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValue: new DateTime(2018, 8, 21, 7, 34, 3, 258, DateTimeKind.Utc)),
                    CreatedById = table.Column<string>(nullable: true),
                    CurrentTotal = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("CompanyStrength_PK_Id", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompanyStrengths_AspNetUsers_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Platoons",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    InitialTotalStrength = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("Platoon_PK_Id", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Ranks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Abbreviation = table.Column<string>(nullable: false),
                    IconUrl = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    level = table.Column<int>(nullable: false, defaultValue: 0)
                        .Annotation("Sqlite:Autoincrement", true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("Rank_PK_Id", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Statuses",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    EndDate = table.Column<DateTime>(nullable: true),
                    Hex = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<string>(nullable: true),
                    level = table.Column<int>(nullable: false, defaultValue: 0)
                        .Annotation("Sqlite:Autoincrement", true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("Status_PK_Id", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Statuses_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PlatoonStrengths",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CompanyStrengthId = table.Column<long>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValue: new DateTime(2018, 8, 21, 7, 34, 3, 423, DateTimeKind.Utc)),
                    CurrentTotal = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PlatoonStrength_PK_Id", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PlatoonStrengths_CompanyStrengths_CompanyStrengthId",
                        column: x => x.CompanyStrengthId,
                        principalTable: "CompanyStrengths",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlatoonCommanders",
                columns: table => new
                {
                    PlatoonId = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PlatoonCommander_CK_PlatoonId_UserId", x => new { x.PlatoonId, x.UserId });
                    table.ForeignKey(
                        name: "FK_PlatoonCommanders_Platoons_PlatoonId",
                        column: x => x.PlatoonId,
                        principalTable: "Platoons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlatoonCommanders_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Sections",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Number = table.Column<int>(nullable: false),
                    PlatoonId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("Section_PK_Id", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sections_Platoons_PlatoonId",
                        column: x => x.PlatoonId,
                        principalTable: "Platoons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlatoonStrengthMissingUsers",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    PlatoonStrengthId = table.Column<long>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PlatoonStrengthMissingUser_PK_Id", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PlatoonStrengthMissingUsers_PlatoonStrengths_PlatoonStrengthId",
                        column: x => x.PlatoonStrengthId,
                        principalTable: "PlatoonStrengths",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlatoonStrengthMissingUsers_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SectionCommanders",
                columns: table => new
                {
                    SectionId = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("SectionCommander_CK_SectionId_UserId", x => new { x.SectionId, x.UserId });
                    table.ForeignKey(
                        name: "FK_SectionCommanders_Sections_SectionId",
                        column: x => x.SectionId,
                        principalTable: "Sections",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SectionCommanders_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_RankId",
                table: "AspNetUsers",
                column: "RankId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_SectionId",
                table: "AspNetUsers",
                column: "SectionId");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyStrengths_CreatedById",
                table: "CompanyStrengths",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_PlatoonCommanders_UserId",
                table: "PlatoonCommanders",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_PlatoonStrengthMissingUsers_PlatoonStrengthId",
                table: "PlatoonStrengthMissingUsers",
                column: "PlatoonStrengthId");

            migrationBuilder.CreateIndex(
                name: "IX_PlatoonStrengthMissingUsers_UserId",
                table: "PlatoonStrengthMissingUsers",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_PlatoonStrengths_CompanyStrengthId",
                table: "PlatoonStrengths",
                column: "CompanyStrengthId");

            migrationBuilder.CreateIndex(
                name: "Rank_Index_Abbreviation",
                table: "Ranks",
                column: "Abbreviation");

            migrationBuilder.CreateIndex(
                name: "IX_SectionCommanders_UserId",
                table: "SectionCommanders",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "Section_Index_PlatoonId_Number",
                table: "Sections",
                columns: new[] { "PlatoonId", "Number" });

            migrationBuilder.CreateIndex(
                name: "IX_Statuses_UserId",
                table: "Statuses",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Ranks_RankId",
                table: "AspNetUsers",
                column: "RankId",
                principalTable: "Ranks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Sections_SectionId",
                table: "AspNetUsers",
                column: "SectionId",
                principalTable: "Sections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Ranks_RankId",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Sections_SectionId",
                table: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "PlatoonCommanders");

            migrationBuilder.DropTable(
                name: "PlatoonStrengthMissingUsers");

            migrationBuilder.DropTable(
                name: "Ranks");

            migrationBuilder.DropTable(
                name: "SectionCommanders");

            migrationBuilder.DropTable(
                name: "Statuses");

            migrationBuilder.DropTable(
                name: "PlatoonStrengths");

            migrationBuilder.DropTable(
                name: "Sections");

            migrationBuilder.DropTable(
                name: "CompanyStrengths");

            migrationBuilder.DropTable(
                name: "Platoons");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_RankId",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_SectionId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "FourD",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "RankId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "SectionId",
                table: "AspNetUsers");
        }
    }
}
