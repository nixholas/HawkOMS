﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using HawkOMS.Models;
using HawkOMS.Models.Accounting;
using HawkOMS.Models.Grouping;
using HawkOMS.Models.User;

namespace HawkOMS.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<CompanyStrength> CompanyStrengths { get; set; }
        public DbSet<Platoon> Platoons { get; set; }
        public DbSet<PlatoonCommander> PlatoonCommanders { get; set; }
        public DbSet<PlatoonStrength> PlatoonStrengths { get; set; }
        public DbSet<PlatoonStrengthMissingUser> PlatoonStrengthMissingUsers { get; set; }
        public DbSet<Rank> Ranks { get; set; }
        public DbSet<Section> Sections { get; set; }
        public DbSet<SectionCommander> SectionCommanders { get; set; }
        public DbSet<Status> Statuses { get; set; }
        
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            builder.Entity<CompanyStrength>(entity =>
            {
                entity.HasKey(cs => cs.Id).HasName("CompanyStrength_PK_Id");
                entity.Property(cs => cs.Id).ValueGeneratedOnAdd();

                entity.Property(cs => cs.CreatedAt).HasDefaultValue(DateTime.UtcNow);
                entity.Property(cs => cs.CurrentTotal).IsRequired();

                entity.HasOne(cs => cs.CreatedBy).WithMany(cb => cb.CompanyStrengths)
                    .HasForeignKey(cs => cs.CreatedById).OnDelete(deleteBehavior:DeleteBehavior.Cascade);
                entity.HasMany(cs => cs.PlatoonStrengths).WithOne(ps => ps.CompanyStrength)
                    .HasForeignKey(ps => ps.CompanyStrengthId).OnDelete(deleteBehavior: DeleteBehavior.Cascade);
            });

            builder.Entity<Platoon>(entity =>
            {
                entity.HasKey(p => p.Id).HasName("Platoon_PK_Id");
                entity.Property(p => p.Id).ValueGeneratedOnAdd();

                entity.Property(p => p.InitialTotalStrength).IsRequired();
                
                entity.HasMany(p => p.PlatoonCommanders).WithOne(pc => pc.Platoon)
                    .HasForeignKey(pc => pc.PlatoonId).OnDelete(deleteBehavior: DeleteBehavior.Cascade);
            });

            builder.Entity<PlatoonCommander>(entity =>
            {
                entity.HasKey(pc => new {pc.PlatoonId, pc.UserId}).HasName("PlatoonCommander_CK_PlatoonId_UserId");

                entity.HasOne(pc => pc.Platoon).WithMany(p => p.PlatoonCommanders)
                    .HasForeignKey(pc => pc.PlatoonId).OnDelete(deleteBehavior: DeleteBehavior.Cascade);
                entity.HasOne(pc => pc.User).WithMany(u => u.PlatoonCommanders)
                    .HasForeignKey(pc => pc.UserId).OnDelete(deleteBehavior: DeleteBehavior.Cascade);
            });

            builder.Entity<PlatoonStrength>(entity =>
            {
                entity.HasKey(ps => ps.Id).HasName("PlatoonStrength_PK_Id");
                entity.Property(ps => ps.Id).ValueGeneratedOnAdd();

                entity.Property(ps => ps.CreatedAt).HasDefaultValue(DateTime.UtcNow);
                entity.Property(ps => ps.CurrentTotal).IsRequired();

                entity.HasOne(ps => ps.CompanyStrength).WithMany(cs => cs.PlatoonStrengths)
                    .HasForeignKey(ps => ps.CompanyStrengthId).OnDelete(deleteBehavior: DeleteBehavior.Cascade);
                entity.HasMany(ps => ps.PlatoonStrengthMissingUsers).WithOne(psmu => psmu.PlatoonStrength)
                    .HasForeignKey(psmu => psmu.PlatoonStrengthId).OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity<PlatoonStrengthMissingUser>(entity =>
            {
                entity.HasKey(psmu => psmu.Id).HasName("PlatoonStrengthMissingUser_PK_Id");
                entity.Property(psmu => psmu.Id).ValueGeneratedOnAdd();

                entity.HasOne(psmu => psmu.ApplicationUser).WithMany(au => au.PlatoonStrengthMissingUsers)
                    .HasForeignKey(psmu => psmu.UserId).OnDelete(DeleteBehavior.Cascade);
                entity.HasOne(psmu => psmu.PlatoonStrength).WithMany(ps => ps.PlatoonStrengthMissingUsers)
                    .HasForeignKey(psmu => psmu.PlatoonStrengthId).OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity<Rank>(entity =>
            {
                entity.HasKey(r => r.Id).HasName("Rank_PK_Id");
                entity.Property(r => r.Id).ValueGeneratedOnAdd();

                entity.HasIndex(r => r.Abbreviation).HasName("Rank_Index_Abbreviation");
                
                entity.Property(r => r.level).HasDefaultValue(0);
                entity.Property(r => r.Abbreviation).IsRequired();
                entity.Property(r => r.IconUrl).IsRequired(false);

                entity.HasMany(r => r.Users).WithOne(u => u.Rank)
                    .HasForeignKey(u => u.RankId).OnDelete(DeleteBehavior.Restrict);
            });

            builder.Entity<Section>(entity =>
            {
                entity.HasKey(s => s.Id).HasName("Section_PK_Id");
                entity.Property(s => s.Id).ValueGeneratedOnAdd();

                entity.Property(s => s.Number).IsRequired();

                entity.HasIndex(s => new {s.PlatoonId, s.Number}).HasName("Section_Index_PlatoonId_Number");

                entity.HasOne(s => s.Platoon).WithMany(p => p.Sections)
                    .HasForeignKey(s => s.PlatoonId).OnDelete(DeleteBehavior.Cascade);
                entity.HasMany(s => s.SectionUsers).WithOne(su => su.Section)
                    .HasForeignKey(su => su.SectionId).OnDelete(deleteBehavior: DeleteBehavior.Restrict);
                entity.HasMany(s => s.SectionCommanders).WithOne(sc => sc.Section)
                    .HasForeignKey(sc => sc.SectionId).OnDelete(DeleteBehavior.Restrict);
            });

            builder.Entity<SectionCommander>(entity =>
            {
                entity.HasKey(sc => new {sc.SectionId, sc.UserId}).HasName("SectionCommander_CK_SectionId_UserId");

                entity.HasOne(sc => sc.Section).WithMany(s => s.SectionCommanders)
                    .HasForeignKey(sc => sc.SectionId).OnDelete(DeleteBehavior.Cascade);
                entity.HasOne(sc => sc.User).WithMany(u => u.SectionCommanders)
                    .HasForeignKey(sc => sc.UserId).OnDelete(DeleteBehavior.Restrict);
            });

            builder.Entity<Status>(entity =>
            {
                entity.HasKey(s => s.Id).HasName("Status_PK_Id");
                entity.Property(s => s.Id).ValueGeneratedOnAdd();

                entity.Property(s => s.StatusType).HasDefaultValue(StatusType.ALLCLEAR);
                entity.Property(s => s.Level).HasDefaultValue(0);
                entity.Property(s => s.Hex).IsRequired(false);
                entity.Property(s => s.Name).IsRequired();
                entity.Property(s => s.StartDate).IsRequired();
                entity.Property(s => s.EndDate).IsRequired(false);

                entity.HasOne(s => s.User).WithMany(u => u.Statuses)
                    .HasForeignKey(s => s.UserId).OnDelete(DeleteBehavior.Restrict);
            });
        }
    }
}